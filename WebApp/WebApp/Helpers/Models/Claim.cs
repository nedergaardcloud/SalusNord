﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApp.Helpers.Models
{
    public class Claim
    {
        public string type { get; set; }
        public string value { get; set; }
    }
}