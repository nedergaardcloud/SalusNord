﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApp.Helpers.Models
{
    public class AccountResponse
    {
        public string name { get; set; }
        public string email { get; set; }
        public List<Claim> claims { get; set; }
    }
}