﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApp.Helpers.Toast
{
    public enum ToastType
    {
        Error,
        Info,
        Success,
        Warning
    }
}