﻿using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using WebApp.Helpers.Models;
using WebApp.Models;

namespace WebApp.Helpers
{
    public static class LoginClient
    {
        private static RestClient client = new RestClient();
        private static readonly Uri baseUrl = new Uri("http://localhost:23362/{call}");

        #region Login
        public static TokenForSession Login(string username, string password)
        {
            client.BaseUrl = baseUrl;
            IRestRequest request = new RestRequest();
            request.RequestFormat = DataFormat.Json;
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");

            request.AddParameter("grant_type", "password");
            request.AddParameter("username", username);
            request.AddParameter("password", password);

            request.Method = Method.POST;
            request.AddUrlSegment("call", "login");

            var response = client.Execute<TokenResponse>(request);

            if (response?.Data == null) return null;

            var tokenForSession = new TokenForSession
            {
                Token = response.Data.access_token,
                TokenType = response.Data.token_type,
                Expires = DateTime.Now.AddSeconds(response.Data.expires_in)
            };

            return tokenForSession;
        }

        public static AccountViewModel GetAccountDetails(string token)
        {
            client.BaseUrl = baseUrl;
            IRestRequest request = new RestRequest();
            request.RequestFormat = DataFormat.Json;
            request.AddHeader("Authorization", $"Bearer {token}");

            request.Method = Method.GET;
            request.AddUrlSegment("call", "api/accounts");

            var response = client.Execute<AccountResponse>(request);

            if (response?.StatusCode != HttpStatusCode.OK)
            {
                throw new HttpException((int)response.StatusCode, ParseErrorMessage(response.Content));
            }

            var role = response.Data.claims.FirstOrDefault(r => r.type.Contains("role"));

            return new AccountViewModel
            {
                Name = response.Data.name,
                Email = response.Data.name,
                Role = role?.value
            };
        }

        #endregion

        private static string ParseErrorMessage(string message)
        {
            return JObject.Parse(message).SelectToken("message").ToString();
        }
    }
}