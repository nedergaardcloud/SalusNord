﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApp.Models
{
    public class ContactPageViewModel
    {
        public List<ContactGroupViewModel> ContactGroups { get; set; }
        public List<ContactViewModel> Contacts { get; set; }
    }
}