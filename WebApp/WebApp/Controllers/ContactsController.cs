﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebApp.Helpers;
using WebApp.Helpers.Toast;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class ContactsController : Controller
    {
        // GET: Contacts
        public async Task<ActionResult> Index(string errorMessage = "")
        {
            var contacts = await APIClient.GetAllContacts(Session["AccessToken"].ToString());
            var contactGroups = await APIClient.GetAllGroups(Session["AccessToken"].ToString());

            var groupList = new List<SelectListItem>()
            {
                new SelectListItem { Text = "Ingen", Value = "Ingen"}
            };

            foreach (var group in contactGroups)
            {
                groupList.Add(new SelectListItem
                {
                    Text = group.Name,
                    Value = group.Id.ToString()
                });
            }
            ViewBag.groupList = groupList;

            return View(contacts);
        }

        [HttpPost]
        public ActionResult Index(string id, string name, string email, string phone, string groupList = "")
        {
            try
            {
                int idAsInt = 0;
                Int32.TryParse(id, out idAsInt);

                ContactViewModel model = new ContactViewModel
                {
                    Id = idAsInt,
                    Name = name,
                    Email = email,
                    Phone = phone
                };

                try
                {
                    var res = APIClient.EditContact(Session["AccessToken"].ToString(), idAsInt, model, groupList);

                    if (res.StatusCode == HttpStatusCode.OK)
                    {
                        this.AddToastMessage("Succes", "Kontaktpersonen er opdateret.", ToastType.Success);

                    }
                }
                catch (HttpException e)
                {
                    this.AddToastMessage("Fejl!", e.Message, ToastType.Error);
                    return RedirectToAction("Index");
                }

                return RedirectToAction("Index");

            }
            catch (HttpException e)
            {
                return RedirectToAction("Index", new { errorMessage = e.Message });
            }
        }

        // GET: Contacts/Details/5
        public ActionResult Details(int id)
        {
            try
            {
                var contact = APIClient.GetContact(Session["AccessToken"].ToString(), id);

                return View(contact);

            }
            catch (HttpException e)
            {
                ViewBag.ErrorMessage = e.Message;
                this.AddToastMessage("Fejl!", e.Message, ToastType.Error);

                return View();
            }
        }

        // GET: Contacts/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Contacts/Create
        [HttpPost]
        public ActionResult Create(ContactViewModel model)
        {
            try
            {
                var res = APIClient.CreateContact(Session["AccessToken"].ToString(), model);

                if (res.StatusCode == HttpStatusCode.OK)
                {
                    this.AddToastMessage("Succes", "Medarbejderen blev oprettet.", ToastType.Success);
                }

                return RedirectToAction("Index");
            }
            catch (HttpException e)
            {
                ViewBag.ErrorMessage = e.Message;
                this.AddToastMessage("Fejl", e.Message, ToastType.Error);

                return View(model);
            }
        }

        // GET: Contacts/Edit/5
        public ActionResult Edit(int id)
        {
            try
            {
                var contact = APIClient.GetContact(Session["AccessToken"].ToString(), id);
                return View(contact);
            }
            catch (HttpException e)
            {
                ViewBag.ErrorMessage = e.Message;
                this.AddToastMessage("Fejl", e.Message, ToastType.Error);

                return View();
            }
        }

        // POST: Contacts/Edit/5
        [HttpPut]
        public ActionResult Edit(int id, ContactViewModel model)
        {
            try
            {
                var res = APIClient.EditContact(Session["AccessToken"].ToString(), id, model, "");

                if (res.StatusCode == HttpStatusCode.OK)
                {
                    this.AddToastMessage("Succes", "Medarbejderens oplysninger er blevet ændret.", ToastType.Success);
                }

                return RedirectToAction("Index");
            }
            catch (HttpException e)
            {
                ViewBag.ErrorMessage = e.Message;
                this.AddToastMessage("Fejl", e.Message, ToastType.Error);

                return View(model);
            }
        }

        // GET: Contacts/Delete/5
        public ActionResult Delete(int id)
        {
            try
            {
                var contact = APIClient.GetContact(Session["AccessToken"].ToString(), id);

                return View(contact);
            }
            catch (HttpException e)
            {
                ViewBag.ErrorMessage = e.Message;
                this.AddToastMessage("Fejl", e.Message, ToastType.Error);

                return View();
            }
        }

        // POST: Contacts/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, ContactViewModel model)
        {
            try
            {
                var res = APIClient.DeleteContact(Session["AccessToken"].ToString(), id);

                if (res.StatusCode == HttpStatusCode.OK)
                {
                    this.AddToastMessage("Succes!", "Kontaktoplysningerne er blevet slettet.", ToastType.Success);
                }

                return RedirectToAction("Index");
            }
            catch (HttpException e)
            {
                ViewBag.ErrorMessage = e.Message;
                this.AddToastMessage("Fejl", e.Message, ToastType.Error);

                return View();
            }
        }
    }
}
