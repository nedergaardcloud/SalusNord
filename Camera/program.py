import RPi.GPIO as GPIO
import time
from picamera import PiCamera
import cv2
import requests
import os
import shutil

GPIO.setmode(GPIO.BCM)

pirPin = 7

GPIO.setup(pirPin, GPIO.IN, GPIO.PUD_UP)
camera = PiCamera()

counter = 1
while True:
	if GPIO.input(pirPin) == GPIO.HIGH:
		print('Motion Detected!')
		try:
			camera.start_preview()
			#Capture image:
			camera.capture('/home/pi/faceDetection/pics/pic%s.jpg' % counter)
			camera.stop_preview()

			#OpenCV: Read image and convert to grayscale:
			image = cv2.imread('/home/pi/faceDetection/pics/pic%s.jpg' % counter)
			gray_img = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

			#Load the Haar classifier:
			# haar_face_cascade = cv2.CascadeClassifier('/home/pi/opencv-3.4.3/data/haarcascades/haarcascade_frontalface_alt.xml')
			# haar_face_cascade = cv2.CascadeClassifier('/home/pi/opencv-3.4.3/data/haarcascades/haarcascade_fullbody.xml')
			haar_face_cascade = cv2.CascadeClassifier('/home/pi/opencv-3.4.3/data/haarcascades/haarcascade_profileface.xml')


			print('Detecting faces...')
			faces = haar_face_cascade.detectMultiScale(gray_img, scaleFactor=1.1, minNeighbors=5)
			print('Faces found: ', len(faces))

			if len(faces) > 0:

				face_counter = 1
				for x, y, w, h in faces:
					crop_img = image[y:y+h, x:x+w]
					cv2.imwrite('croppedface%s.jpg' % face_counter, crop_img)

					print('Loggin in...')
					login_data = {}
					login_data['username'] = 'rnp@promus-it.dk'
					login_data['password'] = '#PromusIT2018'
					login_data['grant_type'] = 'password'
					login = requests.post('https://srv.promus-it.dk/salusnord/login', login_data)

					token = login.json()['access_token']

					print('Sending image for comparison...')
					url = 'https://srv.promus-it.dk/salusnord/compare'
					files = { 'file': ('croppedface%s.jpg' % face_counter, open('croppedface%s.jpg' % face_counter, 'rb'), 'multipart/form-data')}
					headers = {}
					headers['Authorization'] = 'Bearer %s' % token

					r = requests.post(url, files=files, headers=headers)
					if (r.status_code == requests.codes.ok):
						print('Face recognized!')
					face_counter += 1

				cv2.destroyAllWindows()
				

			if os.path.exists('/home/pi/faceDetection/pics/pic%s.jpg' % counter):
  				os.remove('/home/pi/faceDetection/pics/pic%s.jpg' % counter)
			else:
  				print('The file does not exist')
			
			counter += 1
			print('Loop finished!')
		except Exception as e:
			print(e)
	else:
		print('No Motion!')
	time.sleep(1)
