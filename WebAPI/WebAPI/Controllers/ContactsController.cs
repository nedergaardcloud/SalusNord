﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web.Http;
using WebAPI.DAL;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    public class ContactsController : ApiController
    {

        private UnitOfWork uow = new UnitOfWork();

        [HttpGet]
        [Route("contacts/all")]
        public async Task<IHttpActionResult> GetAllContacts()
        {
            var nursingHome = await GetNursingHome();
            var contacts = await uow.ContactRepository.Get(c => c.NursingHome.Id == nursingHome.Id, includeProperties: "ContactGroup");

            return Ok(contacts);
        }

        [HttpGet]
        [Route("contacts/{id}")]
        public async Task<IHttpActionResult> GetContact(int id)
        {
            var contact = await uow.ContactRepository.GetById(id);

            if (contact == null)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.NotFound, "Kontakten blev ikke fundet."));
            }

            return Ok(contact);
        }

        [HttpPost]
        [Route("contacts")]
        public async Task<IHttpActionResult> CreateContact([FromBody]FormDataCollection request)
        {
            var name = request.Get("name");
            var email = request.Get("email");
            var phone = request.Get("phone");
            var nursingHome = await GetNursingHome();

            await uow.ContactRepository.Create(new Contact
            {
                Name = name,
                Email = email,
                Phone = phone,
                NursingHome = nursingHome
            });

            await uow.Save();

            return Ok();
        }


        [HttpPut]
        [Route("contacts/{id}")]
        public async Task<IHttpActionResult> EditContact(int id, [FromBody]FormDataCollection request)
        {
            var name = request.Get("name");
            var email = request.Get("email");
            var phone = request.Get("phone");
            var group = request.Get("group");

            var contact = await uow.ContactRepository.GetById(id);

            if (contact == null)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.NotFound, "Kontakten blev ikke fundet."));
            }

            contact.Name = name;
            contact.Email = email;
            contact.Phone = phone;
            if (group != null && group != "")
            {
                try
                {
                    int groupId;
                    Int32.TryParse(group, out groupId);
                    contact.ContactGroup = await uow.ContactGroupRepository.GetById(groupId);

                }
                catch (Exception e)
                {
                    throw;
                }
            }

            await uow.ContactRepository.Update(contact);
            var res = await uow.Save();

            if (res == 0)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError,
                    "Der skete en fejl. Det var ikke muligt at ændre oplysningerne på denne kontakt."));
            }

            return Ok();
        }

        [HttpDelete]
        [Route("contacts/{id}")]
        public async Task<IHttpActionResult> DeleteContact(int id)
        {
            var contact = await uow.ContactRepository.GetById(id);

            if (contact == null)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.NotFound, "Kontakten blev ikke fundet."));
            }

            await uow.ContactRepository.Delete(id);
            var res = await uow.Save();

            if (res == 0)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError,
                    "Der skete en fejl. Det var ikke muligt at slette den angivne kontakt."));
            }

            return Ok();
        }

        public async Task<NursingHome> GetNursingHome()
        {
            return await Task.Run(() => uow.NursingHomeRepository.dbSet.FirstOrDefault(x => x.User.Email == User.Identity.Name));
        }
    }

}
