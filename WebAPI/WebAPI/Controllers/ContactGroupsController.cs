﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web.Http;
using WebAPI.DAL;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    public class ContactGroupsController : ApiController
    {
        private UnitOfWork uow = new UnitOfWork();

        [HttpGet]
        [Route("groups/all")]
        public async Task<IHttpActionResult> GetAllGroups()
        {
            var nursingHome = await GetNursingHome();
            var groups = await uow.ContactGroupRepository.Get(g => g.NursingHome.Id == nursingHome.Id);

            return Ok(groups);
        }

        [HttpGet]
        [Route("groups/{id}")]
        public async Task<IHttpActionResult> GetGroup(int id)
        {
            var group = await uow.ContactGroupRepository.GetById(id);

            if (group == null)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.NotFound, "Kontakten blev ikke fundet."));
            }

            return Ok(group);
        }

        [HttpPost]
        [Route("groups")]
        public async Task<IHttpActionResult> CreateGroup([FromBody]FormDataCollection request)
        {
            var name = request.Get("name");
            var nursingHome = await GetNursingHome();

            await uow.ContactGroupRepository.Create(new ContactGroup
            {
                Name = name,
                NursingHome = nursingHome
            });

            await uow.Save();

            return Ok();
        }


        [HttpPut]
        [Route("groups/{id}")]
        public async Task<IHttpActionResult> EditGroup(int id, [FromBody]FormDataCollection request)
        {
            var name = request.Get("name");

            var group = await uow.ContactGroupRepository.GetById(id);

            if (group == null)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.NotFound, "Kontakten blev ikke fundet."));
            }

            group.Name = name;
            
            await uow.ContactGroupRepository.Update(group);
            var res = await uow.Save();

            if (res == 0)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError,
                    "Der skete en fejl. Det var ikke muligt at ændre oplysningerne på denne kontakt."));
            }

            return Ok();
        }

        [HttpDelete]
        [Route("groups/{id}")]
        public async Task<IHttpActionResult> DeleteGroup(int id)
        {
            var contact = await uow.ContactGroupRepository.GetById(id);

            if (contact == null)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.NotFound, "Kontaktgruppen blev ikke fundet."));
            }

            await uow.ContactGroupRepository.Delete(id);
            var res = await uow.Save();

            if (res == 0)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError,
                    "Der skete en fejl. Det var ikke muligt at slette den angivne kontakt."));
            }

            return Ok();
        }

        public async Task<NursingHome> GetNursingHome()
        {
            return await Task.Run(() => uow.NursingHomeRepository.dbSet.FirstOrDefault(x => x.User.Email == User.Identity.Name));
        }

    }
}
