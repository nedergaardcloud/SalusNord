﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using WebAPI.DAL;
using WebAPI.Helpers;
using WebAPI.Models;
using WebAPI.Providers;

namespace WebAPI.Controllers
{
    //[Authorize(Roles = "NursingHome")]
    public class ResidentsController : ApiController
    {
        private UnitOfWork uow = new UnitOfWork();

        [HttpGet]
        [Route("residents/all")]
        public async Task<IHttpActionResult> GetAll()
        {
            var nursingHome = uow.NursingHomeRepository.dbSet.FirstOrDefault(x => x.User.Email == User.Identity.Name);
            var residents = await uow.ResidentRepository.Get(r => r.NursingHome.Id == nursingHome.Id, includeProperties: "ContactGroup, Contact");

            return Ok(residents);
        }

        [HttpGet]
        [Route("residents/{id}")]
        public async Task<IHttpActionResult> GetResident(int id)
        {
            var resident = await uow.ResidentRepository.GetById(id);

            if (resident == null)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.NotFound, "Kontakten blev ikke fundet."));
            }

            return Ok(resident);
        }

        [HttpPost]
        [Route("residents")]
        public async Task<IHttpActionResult> CreateResident()
        {
            var fileUploadPath = HttpContext.Current.Server.MapPath("~/TemporaryFiles");
            var multiFormDataStreamProvider = new MultiFileUploadProvider(fileUploadPath);

            await Request.Content.ReadAsMultipartAsync(multiFormDataStreamProvider);

            string uploadingFileName = multiFormDataStreamProvider.FileData.Select(x => x.LocalFileName).FirstOrDefault();

            FileUploadDetails file = new FileUploadDetails
            {
                FilePath = uploadingFileName,
                FileName = Path.GetFileName(uploadingFileName),
                FileLength = new FileInfo(uploadingFileName).Length,
                FileCreatedTime = DateTime.Now.ToLongDateString()
            };

            await ZipClient.Decompress(file.FilePath, fileUploadPath);

            string unzippedDirectory = file.FilePath.Remove(file.FilePath.Length - 4, 4);
            string[] subDirectories = Directory.GetDirectories(unzippedDirectory);

            NursingHome nursingHome = await GetNursingHome();
            string nursingHomeName = nursingHome.Name.Replace(" ", "").ToLower();

            if (nursingHome != null)
            {
                foreach (var dir in subDirectories)
                {
                    string[] dirInfo = Path.GetFileName(dir).Split('-');
                    string name = dirInfo[0];
                    string room = dirInfo[1];

                    await uow.ResidentRepository.Create(new Resident
                    {
                        Name = name,
                        NursingHome = nursingHome,
                        Room = room
                    });

                    await uow.Save();
                    // THIS IS NOT GOOD CODE:
                    var res = (await uow.ResidentRepository.GetAll()).FirstOrDefault(x => x.Name == name && x.Room == room && x.NursingHome == nursingHome);
                    // NOT GOOD CODE ENDED.
                    await new FaceAPIClient().CreatePersonGroupPerson(nursingHomeName, res.Id, res.Name, res.Room, dir);

                }
            }

            var trainingStatus = await new FaceAPIClient().TrainPersonGroup(nursingHomeName);
            if (trainingStatus == Microsoft.Azure.CognitiveServices.Vision.Face.Models.TrainingStatusType.Succeeded)
            {
                File.Delete(file.FilePath);
                var tempFolder = new FileInfo(file.FilePath).Directory.FullName;
                var fileWithoutExt = Path.GetFileNameWithoutExtension(file.FilePath);
                var pathToDelete = Path.Combine(tempFolder, fileWithoutExt);
                Directory.Delete(pathToDelete, true);

                return Ok(trainingStatus.ToString());
            }

            return InternalServerError();
        }

        [HttpPut]
        [Route("residents/{id}")]
        public async Task<IHttpActionResult> EditResident(int id, [FromBody]FormDataCollection request)
        {
            var name = request.Get("name");
            var room = request.Get("room");
            var groupName = request.Get("group");
            var contactName = request.Get("contact");
            var updateContactInfo = request.Get("update");

            var group = uow.ContactGroupRepository.dbSet.FirstOrDefault(g => g.Name == groupName);
            var contact = uow.ContactRepository.dbSet.FirstOrDefault(c => c.Name == contactName);

            var resident = await uow.ResidentRepository.GetById(id);

            if (resident == null)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.NotFound, "Kontakten blev ikke fundet."));
            }

            resident.Name = name;
            resident.Room = room;

            if (updateContactInfo.Equals("true"))
            {
                resident.ContactGroup = group;
                resident.Contact = contact;
            }

            await uow.ResidentRepository.Update(resident);
            var res = await uow.Save();

            if (res == 0)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError,
                    "Der skete en fejl. Det var ikke muligt at ændre oplysningerne på denne kontakt."));
            }

            return Ok();
        }

        [HttpDelete]
        [Route("residents/{id}")]
        public async Task<IHttpActionResult> DeleteResident(int id)
        {
            NursingHome nursingHome = await GetNursingHome();
            string nursingHomeName = nursingHome.Name.Replace(" ", "").ToLower();
            await new FaceAPIClient().DeleteResident(nursingHomeName, id);

            var resident = await uow.ResidentRepository.GetById(id);

            if (resident == null)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.NotFound, "Kontakten blev ikke fundet."));
            }

            await uow.ResidentRepository.Delete(resident);
            var res = await uow.Save();

            if (res == 0)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError,
                    "Der skete en fejl. Det var ikke muligt at slette den angivne kontakt."));
            }

            return Ok();
        }

        private async Task<NursingHome> GetNursingHome()
        {
            return await Task.Run(() => uow.NursingHomeRepository.dbSet.FirstOrDefault(x => x.User.Email == User.Identity.Name));
        }
    }
}
