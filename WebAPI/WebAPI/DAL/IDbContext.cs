﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace WebAPI.DAL
{
    public interface IDbContext<T> : IDisposable where T : class
    {
        Task<T> Create(T entity);
        Task<T> Get(int id);
        Task<List<T>> GetAll();
        Task<T> Update(T entity);
        Task<T> Delete(int id);
        Task Save();
    }
}