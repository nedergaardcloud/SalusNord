﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WebAPI.Models;

namespace WebAPI.DAL
{
    public class DbContextSalusNord : IdentityDbContext<ApplicationUser>
    {
        public DbContextSalusNord() : base("SalusNord")
        {
            Configuration.LazyLoadingEnabled = false;

        }

        public DbSet<Provider> Providers { get; set; }
        public DbSet<NursingHome> NursingHomes { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<ContactGroup> ContactGroups { get; set; }
        public DbSet<Resident> Residents { get; set; }
    }
}